# Cloud Security Contest for SupCom students


### Forewards
Dear attendants for the Sup�Com annual forum of 2018, thank you for your interest in taking this Cloud Security contest provided by Infor Retail Cloud Operations team. We are going to ask   you   to   answer  a couple of questions regarding a given situation and provide some action points. Enjoy and good luck !!


### Introduction
Our application delivery team is willing to design and host a distributed application on AWS (Amazon Web Services) cloud using different AWS services and various tools as show in the schema below:
![challenge.png](./pics/challenge.png)

Your job is to answer a set of questions in order to assist the application delivery team on designing the infrastructure layer. Please send you answers by email to [DL-PDX-OPS-TN@infor.com](mailto:DL-PDX-OPS-TN@infor.com).


### Questions
- You were asked to assist the application delivery team in designing the application in a highly available and secure fashion for production use. What improvements would you recommend to ensure that ? Which AWS component you will need to manage and  control network traffic ? Which AWS service can be used to manage users access with different roles (levels of permissions) ?

- The Cloud Operations team are working on various cost control initiatives in order to minimize waste. How can improve the given architecture to make it cost effective ?

- Part of the ORT (Operational Readiness Test) most important tracks is to deliver a consistent Disaster Recovery plan with the principle of being able to recover from application and infrastructure outages with a minimum downtime, and be able to rollback the application to a previous state up to 30 days back (a hard requirement). Elaborate on what would be your plan for Disaster Recovery, and which components you will use for backups (snapshots) to the EC2 instances and the RDS database.

- While performing UAT (User Acceptance Testing) sessions on customer end and on different periods of the week, it was noticed that that load on the EC2 instances is very high during business hours (9:00AM to 6:00PM EST), and can go unexpectedly higher on specific events (Black friday, ..), whereas the load is low outside of business hours and EC2 instances are completely idle on weekend. Provide a plan in order to scale up and down the number of EC2 instances that are registered to the Elastic Load Balancer in the graph, and which EC2 component you will need to use. Can you give an example of an infrastructure monitoring metric that can be used for triggering scaling-in/scaling-down events ?

- How would you protect the application against DDoS attacks ?

- One of the challenges faced by the application delivery team was to make sure that one of the backend services running in the EC2 instances are running correctly across multiple linux-based OSs with different package managers (debian based, redhat based, ..) and without having to deal with the dependencies nightmare for each system separately. How can this be done ?

- Describe how would you manage users backend access to the application servers while ensuring authentication and access granularity ?


### Practical challenge
One former Infor Retail employees made a basic mistake by committing IAM access keys in code base inside the following public repository: https://bitbucket.org/Predictix/supcom-challenge-2018

An attacker made use of the information in order to gain access to the AWS account where the described application will be hosted, and compromised sensitive information from the RDS mysql database. 
Your job is to find out the password for the root account of the RDS mysql database and any other useful information 
Flag formats should be in format: **_Inf0r.._**

What important lesson can we learn from this event ?
